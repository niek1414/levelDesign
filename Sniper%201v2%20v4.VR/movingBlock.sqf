_N =  _this select 0;
_X1 = _this select 1;
_Y =  _this select 2;
_Noffset =  _this select 3;
_Xoffset =  _this select 4;
_Yoffset =  _this select 5;
_rotation = _this select 6;
_rawMaxSpeed = _this select 7;
_obj = _this select 8;

 bbr = boundingBoxReal  _obj;
 p1 =  bbr select 0;
 p2 =  bbr select 1;
 maxWidth = abs (( p2 select 0) - ( p1 select 0));
 maxLength = abs (( p2 select 1) - ( p1 select 1));
 maxHeight = abs (( p2 select 2) - ( p1 select 2));

_W= maxWidth;
_L= maxLength;
_H= maxHeight;
_var = [];
_var resize _N*_X1*_Y;
_oldPos= getPos _obj;
_offsetSum = (abs _Noffset)+(abs _Xoffset)+(abs _Yoffset);

_Xstart = _oldPos select 0;
_Ystart = _oldPos select 1;
_Zstart = _oldPos select 2;
_Xend = (_oldPos select 0) + _Xoffset;
_Yend = (_oldPos select 1) + _Yoffset;
_Zend = (_oldPos select 2) + _Noffset;

_count = 0;
_var set [_count,_obj];
for [{_k=0}, {_k<_X1}, {_k=_k+1}]do{
	
	for [{_j=0}, {_j<_Y}, {_j=_j+1}]do{
		
		for [{_i=0}, {_i<_N}, {_i=_i+1}]do{ 
			_count = _count + 1;
			
			_calcW =  ((_k)*(_W));
			_calcL =  ((_j)*(_L));
			_calcH =  ((_i)*(_H));
			
            _var set [_count,typeOf _obj createVehicle position _obj];
			(_var select _count) setDir getDir _obj;
			(_var select _count) setPosASL [ ((getPosASL _obj) select 0) + _calcW, ((getPosASL _obj) select 1) + _calcL, ((getPosASL _obj) select 2) + _calcH];
			
			//hint format["%1, %2, %3",_calcL, getPos _obj select 1,(getPos _obj select 1) + _calcL];
		};
	};
};
_back = true;
_progress = 0;
_maxSpeed = 0;
_progressMax = 0;
while {true}
do{
	_maxSpeed = _rawMaxSpeed/diag_fps;
	_progressMax = (_offsetSum / _maxSpeed);
	_Xloc=0;
	_Yloc=0;
	_Zloc=0;
	_Xspeed=0;
	_Yspeed=0;
	_Zspeed=0;
	if (_back)then{
		_progress = _progress + 1;
		if (_progress>_progressMax)then{
			_Xloc = _Xend;
			_Yloc = _Yend;
			_Zloc = _Zend;
			_progress = _progressMax;
			_back = false;
		} else {
			_temp = (_Xoffset/_offsetSum);
			_Xspeed = _temp*_maxSpeed;
			_temp = (_Yoffset/_offsetSum);
			_Yspeed = _temp*_maxSpeed;
			_temp = (_Noffset/_offsetSum);
			_Zspeed = _temp*_maxSpeed;

		}
	} else {
		_progress = _progress - 1;
		if (_progress<0)then{
			_Xloc = _Xstart;
			_Yloc = _Ystart;
			_Zloc = _Zstart;
			_progress = 0;
			_back = true;
		} else {
			_temp = (_Xoffset/_offsetSum);
			_Xspeed = -(_temp*_maxSpeed);
			_temp = (_Yoffset/_offsetSum);
			_Yspeed = -(_temp*_maxSpeed);
			_temp = (_Noffset/_offsetSum);
			_Zspeed = -(_temp*_maxSpeed);
			
		}
	};
	//hint format["%1, %2, %3, %4",_progress, _progressMax, _Xspeed+_Yspeed+_Zspeed, _back];
	
	_count = 0;
	{
		_count = _count + 1;
		_Xloc = ((getPosASL _x) select 0) - _Xspeed;
		_Yloc = ((getPosASL _x) select 1) - _Yspeed;
		_Zloc = ((getPosASL _x) select 2) - _Zspeed;
		if (_rotation!=0)then{_x setDir ((getDir _x)+_rotation);};
		_x setPosASL [_Xloc, _Yloc, _Zloc];
		
	} forEach _var;
	sleep 0.02;
};