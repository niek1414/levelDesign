_obj =  _this select 0;
reflectColor =  _this select 1;
ambientColor =  _this select 2;
falloffStartRange =  _this select 3;
intensity =  _this select 4; //0 is bright to (10 dark?)
brightness =  _this select 5; //0 is bright to (10 dark?)
concentrationLinear = _this select 6; //0 ~ 100, 100 is sharp border
concentrationQuadratic = _this select 7; //0 ~ inf, range it will be seen in a fade

light1 setLightBrightness brightness; // Set its brightness
light1 setLightIntensity intensity; //How bright is the light

light1="#lightpoint" createVehicle getPos _obj;
light1 setLightColor reflectColor;
light1 setLightAmbient ambientColor;
light1 setLightAttenuation [falloffStartRange,0,concentrationLinear,concentrationQuadratic];
deleteVehicle _obj;