_N =  _this select 0;
_X =  _this select 1;
_Y =  _this select 2;
_Noffset =  _this select 3;
_Xoffset =  _this select 4;
_Yoffset =  _this select 5;
_obj =_this select 6;

 bbr = boundingBoxReal  _obj;
 p1 =  bbr select 0;
 p2 =  bbr select 1;
 maxWidth = abs (( p2 select 0) - ( p1 select 0));
 maxLength = abs (( p2 select 1) - ( p1 select 1));
 maxHeight = abs (( p2 select 2) - ( p1 select 2));

_W= maxWidth;
_L= maxLength;
_H= maxHeight;
 
for [{_k=0}, {_k<_X}, {_k=_k+1}]do{
	for [{_j=0}, {_j<_Y}, {_j=_j+1}]do{
		for [{_i=0}, {_i<_N}, {_i=_i+1}]do{ 
			_calcW =  ((_k)*(_W+_Xoffset));
			_calcL =  ((_j)*(_L+_Yoffset));
			_calcH =  ((_i)*(_H+_Noffset));

			_var = typeOf _obj createVehicle position _obj;
			_var setDir getDir _obj;
			_var setPosATL [ ((getPos _obj) select 0) + _calcW, ((getPos _obj) select 1) + _calcL, ((getPos _obj) select 2) + _calcH];
			
			//hint format["%1, %2, %3",_calcL, getPos _obj select 1,(getPos _obj select 1) + _calcL];
		}
	}
};