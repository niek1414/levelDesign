runner setVariable ["selections", []];
runner setVariable ["gethit", []];
runner addEventHandler  [   "HandleDamage",{ 
if (_this select 4 != "")then{
   _unit = _this select 0;
   _selections = _unit getVariable ["selections", []];
   _gethit = _unit getVariable ["gethit", []];
   _selection = _this select 1;
   if !(_selection in _selections) then
   {     
		_selections set [count _selections, _selection];
		_gethit set [count _gethit, 0];    
	};
	_i = _selections find _selection;
	_olddamage = _gethit select _i;
	_damage = _olddamage + ((_this select 2) - _olddamage) * 0.25;
	_gethit set [_i, _damage];    _damage;    
} else {
	_unit = _this select 0;
   _selections = _unit getVariable ["selections", []];
   _gethit = _unit getVariable ["gethit", []];
   _selection = _this select 1;
   if !(_selection in _selections) then
   {     
		_selections set [count _selections, _selection];
		_gethit set [count _gethit, 0];    
	};
	_i = _selections find _selection;
	_olddamage = _gethit select _i;
	_damage = _olddamage + ((_this select 2) - _olddamage) * 0;
	_gethit set [_i, _damage];    _damage;   
}
}];
myLight="#lightpoint" createVehicle [0,0,0];
myLight setLightBrightness 0;
myLight setLightColor [1,0,0];
myLight setLightAmbient [0,0,0];
myLight lightAttachObject [runner,[0,0,0]];  
 
removeVest runner;
runner setobjecttexture [0, "blendv2.jpg"];
removeHeadgear runner;
removeAllWeapons runner;

 runner addWeapon "throw";
 runner addMagazine "HandGrenade_Stone";
 runner addMagazine "HandGrenade_Stone";
 runner addMagazine "30Rnd_9x21_Mag";
 runner addWeapon "hgun_PDW2000_Holo_snds_F";
 runner addMagazine "30Rnd_9x21_Mag";
 runner addMagazine "30Rnd_9x21_Mag";
 
millcounter = 0;
switchcounter = 0;
easecounter = 0;
currentLight = 0;
targetLight = 0;
incVal = 0.002;
dicVal = 0.004;

while {true}
do{
	//hint format ["%1,%2,%3",millcounter,switchcounter,easecounter];
	if (millcounter - switchcounter > 50)then{
		switchcounter = millcounter;
		switch (true) 
		do{
			case (abs(speed runner) == 0): {targetLight = 0;};
			case (abs(speed runner) < 3): {targetLight = 0.01;};
			case (abs(speed runner) < 5): {targetLight = 0.05;};
			case (abs(speed runner) < 11): {targetLight = 0.15;};
			case (abs(speed runner) < 15): {targetLight = 0.18;};
			case (abs(speed runner) < 17): {targetLight = 0.23;};
			case (abs(speed runner) > 17): {targetLight = 0.40;};
			default{myLight setLightBrightness 1;};
		};
		
	};
	if (millcounter - easecounter > 10)then{
		easecounter = millcounter;
		if (currentLight!=targetLight)then{
			if (currentLight<targetLight)then{
				currentLight = currentLight + incVal;
				_div = targetLight - currentLight;
				if (_div < 0.005)then{currentLight = targetLight;};
				if (_div > 0.1)then{currentLight = currentLight + incVal/1.5;};
				if (_div > 0.2)then{currentLight = currentLight + incVal/2;};
				myLight setLightBrightness currentLight;
			}else{
				currentLight = currentLight - dicVal;
				_div = targetLight - currentLight;
				if (_div > -0.005)then{currentLight = targetLight;};
				if (_div < -0.1)then{currentLight = currentLight - dicVal/1.5;};
				if (_div < -0.2)then{currentLight = currentLight - dicVal/2;};
				myLight setLightBrightness currentLight;
			};
		};
	};
	millcounter = millcounter + 1;
	sleep 0.001;
	 
};